package com.orchlib.backend.composer

import com.orchlib.backend.composer.persistence.ComposerRowMapper
import org.springframework.boot.SpringBootConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.datasource.DriverManagerDataSource

@Profile("test")
@SpringBootConfiguration
class TestConfig {
    @Bean
    fun jdbcTemplate(): JdbcTemplate {
        val dataSource = DriverManagerDataSource()
        dataSource.setDriverClassName("org.postgresql.Driver")
        dataSource.url = "dataSourceUrl"
        dataSource.username = "dataSourceUsername"
        dataSource.password = "dataSourcePassword"

        return JdbcTemplate(dataSource)
    }

    @Bean
    fun composerRowMapper(): ComposerRowMapper {
        return ComposerRowMapper()
    }
}
